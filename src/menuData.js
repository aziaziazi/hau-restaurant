export const carte = [
  {
    name: "Apéritifs",
    items: [
      { name: "Kir", price: 5 },
      { name: "Vin de lychee", price: 5 },
      { name: "Sherry", price: 5 },
      { name: "Porto", price: 5 },
      { name: "Pineau des Charentes", price: 5 },
      { name: "Martini blanc, rouge", price: 5 },
      { name: "Pastis", price: 9 },
      { name: "Campari soda, orange", price: 9 },
      { name: "Mandarine tonic", price: 9 },
      { name: "Gin tonic", price: 9 },
      { name: "Coupe de Champagne", price: 9 },
      { name: "Kir Royal", price: 9 },
    ],
  },{
    name: "Potages",
    items: [
      { name: "Potage aux cheveux d’ange et au poulet", price:  7 },
      { name: "Potage piquant au poivre oriental", price: 7 },
      { name: "Potage aux raviolis de scampis", price: 8 },
      { name: "Potage saïgonnais", price: 8 },
      { name: "Potage du chef au saté", price: 8 },
    ],
  },{
    name: "Entrées Froides",
    items: [
      { name: "Boeuf cru piquant", price: 13 },
      { name: "Salade de cheveux d’ange", price: 10 },
      { name: "Salade d’ananas et de scampis", price: 12 },
    ],
  },{
    name: "EntréeS vapeurs",
    items: [
      { name: "Bouchées de scampis", price: 8 },
      { name: "Bouchées de porc et de scampis", price:  8 },
      { name: "Boutons de roses", price: 8 },
      { name: "Filet de poulet aux feuilles de bananier", price: 9 },
      { name: "Crêpes tonkinoises", price: 9 },
      { name: "Panaché vapeur", price: 11 },
    ],
  },{
    name: "EntréeS Chaudes",
    items: [
      { name: "Raviolis croustillants", price: 8 },
      { name: "Croquettes vietnamiennes", price: 9 },
      { name: "Brochettes de filet de poulet au saté", price: 8 },
      { name: "Aile de poulet farcie", price: 7 },
      { name: "Croustillons de scampis", price: 9 },
      { name: "Scampis aux épices", price: 12 },
      { name: "Assiette Hau ( 2 personnes )", price: 24 },
    ],
  },{
    name: "Fruits de mer",
    items: [
      { name: "Scampis piquants aux aromates", price: 18 },
    { name: "Scampis au saté et au lait de coco", price: 18 },
      { name: "Scampis grillés au saté", price: 18 },
      { name: "Scampis grillés à la citronnelle", price: 18 },
    ],
  },{
    name: "Poulet",
    items: [
      { name: "Poulet caramélisé au gingembre", price: 14 },
      { name: "Poulet grillé au curry rouge et lait de coco", price: 14 },
      { name: "Poulet grillé à la citronnelle", price: 14 },
    ],
  },{
    name: "Canard",
    items: [
      { name: "Canard cantonnais", price: 18 },
      { name: "Canard à l’impériale", price: 18 },
      { name: "Canard laqué aux crêpes", price: 20 },
      { name: "Magret de canard grillé aux cinq parfums et aux grains de sésame", price: 20 },
    ],
  },{
    name: "Boeuf",
    items: [
      { name: "Boeuf lúc lác", price: 18 },
      { name: "Boeuf piquant au citron vert", price: 18 },
      { name: "Boeuf au gingembre et grains de sésame", price: 18 },
      { name: "Boeuf grillé aux cinq parfums", price: 18 },
    ],
  },{
    name: "Porc",
    items: [
      { name: "Porc aux épices, citronnelle et galanga", price: 14 },
    ],
  },{
    name: "Nouilles et riz",
    items: [
  
      { name: "Riz sauté aux crevettes, porc laqué et poulet", price: 15 },
      { name: "Riz sauté aux scampis", price: 18 },
      { name: "Nouilles aux légumes", price: 12 },
      { name: "Nouilles aux sept merveilles", price: 15 },
    ],
  },{
    name: "Accompagnements",
    items: [
      { name: "Riz sauté aux oeufs", price: 8 },
      { name: "Assiette végétarienne", price: 10 },
      { name: "Nouilles sautées au soja", price: 8 },
    ],
  },{
    name: "Desserts",
    items: [
      { name: "Glace vanille et confit de gingembre", price: 6 },
      { name: "Banane grillée au lait de coco", price: 6.50 },
      { name: "Beignet à la noix de coco", price: 6.50 },
      { name: "Beignet de banane au miel", price: 8 },
      { name: "Assortiment de sorbet", price: 7.50 },
  
    { name: "Supplément de 2,50 euros pour un flambage à l’alcool" },
    ],
  },{
    name: "Boissons",
    items: [
      { name: "Eau plate 0,25 L", price: 3 },
      { name: "Eau plate 0,5 L", price: 5 },
      { name: "Eau plate 1 L", price: 7.50 },
      { name: "San Pellegrino 0,5 L", price: 5 },
      { name: "San Pellegrino 1 L", price: 9 },
  
      { name: "Coca, Coca light, Parasol orange, Ice Tea, Tonic", price: 3 },
      { name: "Jus de pomme Bio", price: 3.50 },
    ],
  },{
    name: "Bières",
    items: [
      { name: "Bertinchamps 0,5 L ( Blonde, Brune, Triple )", price: 9 },
    ],
  },{
    name: "Boissons chaudes",
    items: [
      { name: "Thé & tisane", price: 3 },
      { name: "Café", price: 3.50 },
      { name: "Cappuccino", price: 4 },
      { name: "Irish Coffee", price: 12 },
    ],
  },{
    name: "Info allergènes",
    items: [
      { name: "Chers clients, si vous désirez des informations sur la présence éventuelle d’allergène dans nos produits / préparations veuillez contacter le personnel. Attention la composition des produits et préparations, peut varier. Merci." },
    ],
  }
];

export const WIPvins = [
  {
    name: "les Bulles",
    items: [
      { type: "h3", name: "France"},
      { name: "Manganèse - Domaine richard Pottiers - Méthode Aromatique ( BIO )", price: 36 },
      { name: "champagne Grand cru cramant - « Table Ronde » Lancelot Pienne", price: 52 },
      { type: "h3", name: "Italie"},
      { name: "Blanc de Blanc - Alessandro Viola - Sicilia", price: 39 },
      { name: "Prosecco - Sèttolo - Treviso DOC Fratelli Collavo ( BIO )", price: 31 },
    ]
  },{
    name: "les vins blancs",
    items: [
      { type: "h3", name: "Allemagne"},
      { type: "h3", name: "Autriche"},
      { type: "h3", name: "France"},
      { type: "h4", name: "AlsAce"},
      { name: "Riesling «Elsbourg» - J.F. Otter 2007", price: 36 },
      { type: "h4", name: "BordeAux", price: 36 },
      { name: "Château Grand Abord - Graves 2013", price: 36 },
      { name: "Château Hostens Picant - Sainte-Foy Bordeaux ( Biodynamie) 2012", price: 36 },
      { name: "Château Hostens Picant - Sainte-Foy Bordeaux ( 375ml ) ( Biodynamie ) 2012", price: 36 },
      { type: "h4", name: "Bourgogne", price: 36 },
      { name: "Hautes-Côtes de Nuits ‘Pinot Blanc’’ Domaine de la Douaix 2011", price: 36 },
      { name: "Hautes-Côtes de Nuits ‘’Terres Blondes’’ Domaine de la Douaix 2012", price: 36 },
    ]
  },{
    name: "les Bulles",
    items: [
      { name: "Manganèse - Domaine richard Pottiers - Méthode Aromatique ( BIO )", price: 36 },
      { name: "champagne Grand cru cramant - « Table Ronde » Lancelot Pienne", price: 52 },
      { name: "Blanc de Blanc - Alessandro Viola - Sicilia", price: 39 },
      { name: "Prosecco - Sèttolo - Treviso DOC Fratelli Collavo ( BIO )", price: 31 },
      { name: "Spumante Metodo Classico Brut Frecciarossa 2008", price: 52 },
    ]
  },
];

export const vins = [
  {
    items: [
      { name: "La carte de vin nature et biologique est à découvrir au restaurant."},
    ]
  },
];
